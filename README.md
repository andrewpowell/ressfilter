RESSFilter for Java EE Apps
===========================

Prerequisites
-------------

This filter requires the [WURFL](http://wurfl.sourceforge.net) context listener to be running in your application.

The filter [can be found here](http://wurfl.sourceforge.net/docs/java/net/sourceforge/wurfl/core/web/WURFLServletContextListener.html).
This will create an instance of the WURFL instance in the [ServletContext](http://docs.oracle.com/javaee/6/api/javax/servlet/ServletContext.html).
Once this listener is running, the filter will be able to run on each request.


Usage
-----

Define a filter within your web.xml file for the RESSFilter:

```
<filter>
   <filter-name>RESSFilter</filter-name>
   <filter-class>com.universalmind.web.filters.RESSFilter</filter-class>
</filter>
<filter-mapping>
   <filter-name>RESSFilter</filter-name>
   <url-pattern>/*</url-pattern>
</filter-mapping>
```

What this will do, in the first iteration, is simply place a boolean attribute into each request of type
`RESSConstants.IS_MOBILE` and the WURFL [Device](http://wurfl.sourceforge.net/docs/java/net/sourceforge/wurfl/core/Device.html) object under the attribute `RESSConstants.DEVICE`.  This will allow
you to do device specific functionality in your code without having to introspect the device information.

