package com.universalmind.web.filters;

import com.universalmind.web.constants.RESSConstants;
import net.sourceforge.wurfl.core.Device;
import net.sourceforge.wurfl.core.GeneralWURFLEngine;
import net.sourceforge.wurfl.core.WURFLEngine;
import net.sourceforge.wurfl.core.resource.WURFLResource;
import net.sourceforge.wurfl.core.resource.XMLResource;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;

import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

/**
 * Created by andrewpowell on 2/25/14.
 */
public class RESSFilterTest {

    private MockHttpServletRequest mockHttpServletRequest;
    private MockHttpServletResponse mockHttpServletResponse;
    private MockFilterChain mockFilterChain;

    @Before
    public void setup() {
        WURFLResource root = new XMLResource("classpath:/wurfl.zip");
        WURFLEngine mockWURFLEngine = new GeneralWURFLEngine(root);

        ServletContext mockServletContext = new MockServletContext("/ress", new FileSystemResourceLoader());

        mockHttpServletRequest = new MockHttpServletRequest(mockServletContext);
        mockHttpServletResponse = new MockHttpServletResponse();
        mockFilterChain = new MockFilterChain();

        mockHttpServletRequest.getServletContext()
                .setAttribute(WURFLEngine.class.getName(), mockWURFLEngine);
        mockHttpServletResponse.setOutputStreamAccessAllowed(false);

        mockHttpServletRequest.setMethod("GET");
    }

    @Test
    public void testInit() {
        RESSFilter ressFilter = new RESSFilter();
        try {
            ressFilter.init(mock(FilterConfig.class));
        } catch (ServletException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testDestroy() {
        RESSFilter ressFilter = new RESSFilter();
        try {
            ressFilter.init(mock(FilterConfig.class));
            ressFilter.destroy();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testFilterMobile() {
        String MOBILE_USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350 Safari/8536.25";
        setUserAgent(MOBILE_USER_AGENT);
        processFilter();
        assertTrue(mockHttpServletRequest.getAttribute(RESSConstants.IS_MOBILE).equals(true));
    }

    @Test
    public void testFilterMobileDeviceType() {
        String MOBILE_USER_AGENT = "Mozilla/5.0 (iPhone; CPU iPhone OS 6_1_4 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10B350 Safari/8536.25";
        setUserAgent(MOBILE_USER_AGENT);
        processFilter();
        Device testDevice = (Device) mockHttpServletRequest.getAttribute(RESSConstants.DEVICE);
        assertTrue(testDevice.getDeviceRootId().equals("apple_iphone_ver5"));
    }

    @Test
    public void testFilterDesktopDeviceType() {
        String DESKTOP_USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2";
        setUserAgent(DESKTOP_USER_AGENT);
        processFilter();
        Device testDevice = (Device) mockHttpServletRequest.getAttribute(RESSConstants.DEVICE);
        assertTrue(testDevice.getId().equals("generic_web_browser"));
    }

    @Test
    public void testFilterDesktop() {
        String DESKTOP_USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2";
        setUserAgent(DESKTOP_USER_AGENT);
        processFilter();
        assertTrue(mockHttpServletRequest.getAttribute(RESSConstants.IS_MOBILE).equals(false));
    }

    private void processFilter() {
        RESSFilter ressFilter = new RESSFilter();
        try {
            ressFilter.doFilter(mockHttpServletRequest, mockHttpServletResponse, mockFilterChain);
        } catch (IOException e) {
            fail(e.getMessage());
        } catch (ServletException e) {
            fail(e.getMessage());
        }
    }

    private void setUserAgent(String agentString) {
        String USER_AGENT_HEADER_NAME = "User-Agent";
        mockHttpServletRequest.addHeader(USER_AGENT_HEADER_NAME, agentString);
    }
}
