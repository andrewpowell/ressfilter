package com.universalmind.web.constants;

/**
 * Created by andrewpowell on 2/25/14.
 */
public class RESSConstants {

    /**
     * Is device mobile constant.
     */
    public static String IS_MOBILE = "device_is_mobile";
    public static String DEVICE    = "device_type";

}
