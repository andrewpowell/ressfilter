package com.universalmind.web.filters;

import com.universalmind.web.constants.RESSConstants;
import net.sourceforge.wurfl.core.Device;
import net.sourceforge.wurfl.core.WURFLEngine;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Created by andrewpowell on 2/17/14.
 */
public class RESSFilter implements Filter {

    /**
     *
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     *
     * @param servletRequest
     * @param servletResponse
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        // Obtain an instance of the WURFLEngine
        WURFLEngine engine = (WURFLEngine) servletRequest.getServletContext().getAttribute(WURFLEngine.class.getName());

        // The current device
        Device device = engine.getDeviceForRequest((HttpServletRequest) servletRequest);

        // Put the is mobile flag into the servlet request
        servletRequest.setAttribute(RESSConstants.IS_MOBILE, isMobileBrowser(device));
        servletRequest.setAttribute(RESSConstants.DEVICE, device);
    }

    /**
     *
     */
    @Override
    public void destroy() {

    }

    /**
     *
     */
    public RESSFilter() {
    }

    /**
     *
     * @param device
     * @return
     */
    private boolean isMobileBrowser(Device device) {
        return Boolean.parseBoolean(device.getCapability("is_wireless_device")) && Boolean.parseBoolean(device.getCapability("device_claims_web_support"));
    }
}
